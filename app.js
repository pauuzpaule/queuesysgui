let express = require('express');
let cors = require('cors');
let serveStatic = require('serve-static');
app = express();
app.use(serveStatic(__dirname + "/dist"));
app.get(/.*/, function (req, res) {
    res.sendFile(__dirname+ "/dist/index.html");
});
let port = process.env.PORT || 5000;
let hostname = 'https://test2.sayrunjah.com';

app.listen(port, hostname, () => {
    console.log("Okay..")
    // console.log(`Server running at http://${hostname}:${port}/`);
});