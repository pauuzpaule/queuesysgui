// server.js
let express = require('express');
let path = require('path');
const cors = require('cors');
app = express();
let serveStatic = require('serve-static');
// Your own super cool function
let logger = function(req, res, next) {
    try {
        let jsonBody = JSON.stringify(req);
        console.log("req", req)
    } catch (err) {
        //app.logger.error("content error, error type: invalid json, error msg:" + err);
    }
    next(); // Passing the request to the next handler in the stack.
};
app.use(logger);
app.use(serveStatic(__dirname + "/dist"));
app.get(/.*/, function (req, res) {

    res.sendFile(__dirname+ "/dist/index.html");
});
let port = process.env.PORT || 5000;
// let hostname = 'test2.sayrunjah.com';

//let hostname = '127.0.0.1';
let hostname = '192.168.43.146';

app.listen(port, hostname, () => {
    console.log(`Server running at http://${hostname}:${port}/`);
});