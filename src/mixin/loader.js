import Swal from "sweetalert2";
const Loader = {
    methods: {
        async showLoader(callback, title="Please wait...") {
            Swal.fire({
                title: title,
                allowOutsideClick: () => !Swal.isLoading(),
                onOpen: () => {
                    Swal.showLoading();
                    callback(Swal)
                },

            })
        }
    }
};

export default Loader;
