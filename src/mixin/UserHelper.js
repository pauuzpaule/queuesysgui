const UserHelper = {
    methods: {
        getUser() {
            return localStorage.getItem("user_data") == null ? null : JSON.parse(localStorage.getItem("user_data"))
        },
        isAdmin() {
            return this.getUser() == null ? false : this.getUser().is_superuser;
        }
    }
};

export default UserHelper;