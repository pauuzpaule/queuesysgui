$(document).ready(() => {
  'use strict';

  mdc.autoInit();

  // Ripple for buttons
  let buttons = document.querySelectorAll('.mdc-button');
  for (let i = 0, button; button = buttons[i]; i++) {
    mdc.ripple.MDCRipple.attachTo(button);
  }

  // Focus for textfields
  let textFields = document.querySelectorAll('.mdc-text-field');
  for (let i = 0, textField; textField = textFields[i]; i++) {
    mdc.textField.MDCTextField.attachTo(textField);
  }

  const menuEls = Array.from(document.querySelectorAll('.mdc-menu'));
  menuEls.forEach((menuEl, index) => {
    const menu = new mdc.menu.MDCMenu(menuEl);
    const buttonEl = menuEl.parentElement.querySelector('.mdc-menu-button');
    buttonEl.addEventListener('click', () => {
      menu.open = !menu.open;
    });
    menu.setAnchorCorner(mdc.menu.Corner.BOTTOM_LEFT);
    menu.setAnchorElement(buttonEl)
  });

  // Tabs
  let tabBars = document.querySelectorAll('.mdc-tab-bar');
  for (let i = 0, tabBar; tabBar = tabBars[i]; i++) {
    let currentTabBar = new mdc.tabBar.MDCTabBar(tabBar);
    currentTabBar.listen('MDCTabBar:activated', function(event) {
      let $this = $(this);
      let contentEls = $this.siblings('.content');
      contentEls.map((index, contentEl) => {
        contentEl.classList.remove('content--active');
      });
      contentEls[event.detail.index].classList.add('content--active');
    });
  }
});