import {createRouter, createWebHistory} from 'vue-router'
import Home from '../views/Home.vue'
import auth from "../middleware/auth";
import Login from "../views/Login";
import QueueList from "../views/QueueList";
import ServicePoint from "../views/ServicePoint";
import Users from "../views/Users";
import UserHelper from "../mixin/UserHelper";
import ServicePointSelect from "../views/ServicePointSelect";
import CustomerBook from "../views/CustomerBook";
import DashBoard from "../views/DashBoard";

const routes = [
    {
        path: '/',
        name: 'Login',
        component: Login
    }, {
        path: '/home',
        name: 'Home',
        component: Home,
        meta: {
            middleware: ["auth"],
        },
    }, {
        path: '/queue-list',
        name: 'QueueList',
        component: QueueList,
        meta: {
            middleware: ["auth"],
        },
    },{
        path: '/customer-book',
        name: 'CustomerBook',
        component: CustomerBook,
        meta: {
            middleware: [],
        },
    },{
        path: '/service-point',
        name: 'ServicePoint',
        component: ServicePoint,
        meta: {
            middleware: ["auth", "superuser"],
        },
    },{
        path: '/users',
        name: 'Users',
        component: Users,
        meta: {
            middleware: ["auth", "superuser"],
        },
    },{
        path: '/select-service-point',
        name: 'SelectServicePoint',
        component: ServicePointSelect,
        meta: {
            middleware: ["auth"],
        },
    },
    {
        path: '/dashboard',
        name: 'Dashboard',
        component: DashBoard,
        meta: {
            middleware: ["auth"],
        },

    }
];

const router = createRouter({

    history: createWebHistory(process.env.BASE_URL),
    routes,

});

router.beforeEach((to, from, next) => {
    if (to.meta.middleware) {
        const arr = to.meta.middleware;

        if(arr.includes("superuser")) {
            let isAdmin = UserHelper.methods.isAdmin();

            if(!isAdmin) {
                return next(from.path)
            }
        }

        if (arr.includes("auth")) {

            if (!localStorage.getItem('auth_token')) {
                console.log(localStorage.getItem('auth_token'));
                return next({name: "Login"});
            }

            $.ajaxSetup({
               headers: {
                   'Authorization': 'Token ' + localStorage.getItem('auth_token')
               }
            });
        }
    }

    return  next();
});

export default router
