export default function auth({ next, router }) {
    if (!localStorage.getItem('auth_tokenp')) {
        return router.push({ name: 'Home' });
    }

    return next();
}